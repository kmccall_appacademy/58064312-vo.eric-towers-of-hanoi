class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end


  def play
    until won?
      puts "Select a pile to remove a disc from."
      from_pile = gets.to_i - 1
      puts "Select another pile to put the disc in."
      to_pile = gets.to_i - 1

      if valid_move?(from_tower, to_tower)
        move(from_pile, to_pile)
      else
        puts "Invalid move. Try again."
      end

    end
  end

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

  def valid_move?(from_tower, to_tower)
    return true if @towers[to_tower].empty?

    !@towers[from_tower].empty? &&
    @towers[from_tower].last < @towers[to_tower].last.to_i
  end

  def won?
    @towers[0].empty? && @towers[1..2].any? { |tower| tower == [3, 2 ,1] }
  end

end
